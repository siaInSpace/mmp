//
// Created by sia on 23/04/2020.
//

#ifndef MMP_NEWSTRAIGHTACTION_H
#define MMP_NEWSTRAIGHTACTION_H

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <actionlib/server/simple_action_server.h>
#include <mmp/newStraightMsgAction.h>
#include <tf2_ros/transform_listener.h>

//Straight action server
class NewStraightAction {
    private:
        bool have_start_distance;
        double start_distance;
        tf2::Vector3 vector_to_goal;
        geometry_msgs::TransformStamped current_transform;
        tf2_ros::Buffer buffer;
        tf2_ros::TransformListener tfListener;
        double yaw;
        bool update_transform(std::string goal_frame);
        double get_velocity_factor();

    protected:
        ros::NodeHandle nh_;
        actionlib::SimpleActionServer<mmp::newStraightMsgAction> as_;
        mmp::newStraightMsgFeedback feedback;
        mmp::newStraightMsgResult result;
        std::string action_name;
        ros::Publisher pub_cmd_vel;
    
    public:
        void execute_cb(const mmp::newStraightMsgGoalConstPtr &goal);
        explicit NewStraightAction(std::string name);
};


#endif //MMP_NEWSTRAIGHTACTION_H

/*
 *	File: spin.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/03/2020
 *
 */

#ifndef SPIN_H
#define SPIN_H
#include "action.h"
#include <mmp/spinAction.h>
#include <mmp/newSpinMsgAction.h>
#include <actionlib/client/simple_action_client.h>

class Spin : public Action{
    private:
        double degrees;
        double speed;
        void doneCb(const actionlib::SimpleClientGoalState &state,
                const mmp::newSpinMsgResultConstPtr &result);
        void activeCb();
        void feedbackCb(const mmp::newSpinMsgFeedbackConstPtr &feedback);
        
    protected:
        void json_load(const json &j) override;
    
    public:
        std::string toString() override;
        void act(std::string goal_frame) override;
        Spin(double degrees = 0, double speed = 0);
        Spin(const Spin &other);
        geometry_msgs::Transform get_transform() override;
};

#endif



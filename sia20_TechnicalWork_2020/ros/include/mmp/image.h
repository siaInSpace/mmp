//
// Created by sia on 02/04/2020.
//

#ifndef MMP_IMAGE_H
#define MMP_IMAGE_H
#include <string>
#include "action.h"
#include <sensor_msgs/Image.h>
//Class to take image from camera feed and save to file.
class Image : public Action{
    private:
        std::string path;
        bool done;
        void imageCb(const sensor_msgs::ImageConstPtr &msg);
        
    protected:
        void json_load(const json &j);
        
    public:
        std::string toString();
        void act(const std::string goal_frame);
        Image(std::string path = "images");
        Image(const Image &other);
        geometry_msgs::Transform get_transform();
};


#endif //MMP_IMAGE_H

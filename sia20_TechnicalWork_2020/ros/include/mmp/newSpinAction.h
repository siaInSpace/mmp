//
// Created by sia on 23/04/2020.
//

#ifndef MMP_NEWSPINACTION_H
#define MMP_NEWSPINACTION_H

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <actionlib/server/simple_action_server.h>
#include <mmp/newSpinMsgAction.h>
#include <tf2_ros/transform_listener.h>

//Spin action server
class NewSpinAction {
    private:
        bool have_start_yaw;
        double start_yaw;
        geometry_msgs::TransformStamped current_transform;
        tf2_ros::Buffer buffer;
        tf2_ros::TransformListener tfListener;
        double yaw;
        bool update_transform(std::string goal_frame);
        
    protected:
        ros::NodeHandle nh_;
        actionlib::SimpleActionServer<mmp::newSpinMsgAction> as_;
        mmp::newSpinMsgFeedback feedback;
        mmp::newSpinMsgResult result;
        std::string action_name;
        ros::Publisher pub_cmd_vel;
        
    public:
        void execute_cb(const mmp::newSpinMsgGoalConstPtr &goal);
        explicit NewSpinAction(std::string name);
};


#endif //MMP_NEWSPINACTION_H

/*
 *	File: straight.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/03/2020
 *
 */

#include "mmp/straight.h"
#include "mmp/action.h"
#include "mmp/json.hpp"
using json = nlohmann::json;

#include <ros/ros.h>
#include <mmp/straightAction.h>
#include <actionlib/client/simple_action_client.h>
#include <mmp/newStraightMsgAction.h>

typedef actionlib::SimpleActionClient<mmp::newStraightMsgAction> Client;


Straight::Straight(double distance, double speed){
    this->distance = distance;
    this->speed = speed;
}

Straight::Straight(const Straight& other) : Straight(other.distance, other.speed){
}


void Straight::json_load(const json &j){
    j.at("speed").get_to(this->speed);
    j.at("distance").get_to(this->distance);
}   

std::string Straight::toString(){
    std::string s;
    s += "Straight\n";
    s += "Speed: " + std::to_string(this->speed) + "\n";
    s += "Distance: " + std::to_string(this->distance) + "\n";
    return s;
}

//Print action result
void Straight::doneCb(const actionlib::SimpleClientGoalState &state,
        const mmp::newStraightMsgResultConstPtr &result){
    ROS_INFO("Finished in state: %s", state.toString().c_str());
    ROS_INFO_STREAM("Error: " << result->error);
}

//Print confirmation that goal is active
void Straight::activeCb(){
    ROS_INFO_STREAM("Goal went active");
}

//Print feedback from action server
void Straight::feedbackCb(const mmp::newStraightMsgFeedbackConstPtr &feedback){
    ROS_INFO("Straight percent complete = %03.02f", feedback->percentComplete);
}

//Get transform fot a straight
geometry_msgs::Transform Straight::get_transform() {
    geometry_msgs::Transform temp;
    //Straight only moves in x direction
    temp.translation.x = distance;
    temp.rotation.w = 1.0;
    return  temp;

}

//http://wiki.ros.org/actionlib_tutorials/Tutorials/Writing%20a%20Callback%20Based%20Simple%20Action%20Client
//Call the action server with goal information from this object.
void Straight::act(const std::string goal_frame){
    Client ac("new_straight_action_server", true);
    ac.waitForServer();
    mmp::newStraightMsgGoal goal;
    goal.speed = speed;
    goal.goal_frame = goal_frame;
    goal.reverse = distance < 0;
    ac.sendGoal(goal,
            boost::bind(&Straight::doneCb, this, _1, _2),
            boost::bind(&Straight::activeCb, this),
            boost::bind(&Straight::feedbackCb, this, _1));
    ac.waitForResult();
}


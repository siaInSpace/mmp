//
// Created by sia on 27/03/2020.
//

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <mmp/spinAction.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

//Old action server
class SpinAction{
private:
    geometry_msgs::TransformStamped startPos;
    geometry_msgs::TransformStamped goalPos;
    geometry_msgs::TransformStamped transform_to_goal;
    //Setup tf tree listener and broadcaster
    tf2_ros::Buffer buffer;
    tf2_ros::TransformListener tfListener;
    tf2_ros::TransformBroadcaster tfBroadcaster;
    double yaw;

    void initial_poses(double angle){
        //Wait for a start pose.
        while(true) {
            try {
                startPos = buffer.lookupTransform("odom", "base_link", ros::Time(0));
            }catch (tf2::TransformException &e){
                ROS_WARN("%s", e.what());
                ros::Duration(0.1).sleep();
                continue;
            }
            break;
        }
        startPos.child_frame_id = "start_pos";

        //Calculate the quaternion for the rotation and add a goal pose
        tf2::Quaternion rot;
        rot.setRPY(0, 0, -angle*M_PI/180.0);
        rot.normalize();
        tf2::convert(rot, goalPos.transform.rotation);
        goalPos.child_frame_id = "goal";
        goalPos.header.frame_id = "start_pos";
    }

    bool update_transforms(){
        //update stamp times
        startPos.header.stamp = ros::Time::now();
        tfBroadcaster.sendTransform(startPos);
        goalPos.header.stamp = ros::Time::now();
        tfBroadcaster.sendTransform(goalPos);

        //Look for goal pose in reference to the base link
        try{
            transform_to_goal = buffer.lookupTransform("base_link", "goal", ros::Time(0));
        }catch (tf2::TransformException &e){
            ROS_WARN("%s", e.what());
            ros::Duration(0.1).sleep();
            return false;
        }
        //Get the rotation to the goal pose
        tf2::Quaternion q(transform_to_goal.transform.rotation.x,
                          transform_to_goal.transform.rotation.y,
                          transform_to_goal.transform.rotation.z,
                          transform_to_goal.transform.rotation.w);
        tf2::Matrix3x3 matrix(q);
        double roll, pitch;
        matrix.getRPY(roll, pitch, yaw);
        ROS_INFO_STREAM("Angle to goal: " << yaw*180.0/M_PI);
        return true;
    }

protected:
    ros::NodeHandle _nh;
    actionlib::SimpleActionServer<mmp::spinAction> _as;
    std::string action_name;
    mmp::spinFeedback feedback;
    mmp::spinResult result;
    ros::Publisher pub_cmd_vel;
public:
    void executeCB(const mmp::spinGoalConstPtr &goal){
        ros::Rate rate(100);
        ROS_INFO_STREAM("Executing spin action with degrees: " << goal->degrees
        << " and speed: " << goal->speed);
        geometry_msgs::Twist msg_cmd_vel;
        //Setup tf tree start pos and goal pos
        initial_poses(goal->degrees);
        bool success = true;

        while (ros::ok()){
            //Check if action is preempted
            if(_as.isPreemptRequested()){
                ROS_INFO_STREAM("Preempted " << action_name);
                _as.setPreempted();
                success = false;
                break;
            }
            if (!update_transforms()) continue; //unable to update transforms, re run loop.

            //Make sure to move the correct way
            msg_cmd_vel.angular.z = yaw < 0 ? -goal->speed : goal->speed;
            msg_cmd_vel.angular.z = msg_cmd_vel.angular.z/100;
            ROS_INFO_STREAM("Moving at: " << msg_cmd_vel.angular.z);

            //Check if we are close enough to the goal pose
            if (abs(yaw) < 0.5*M_PI/180) break;
            pub_cmd_vel.publish(msg_cmd_vel);

            //Calculate and publish feedback
            //Calculation is a bit weird as the turning is inverted to be correct with respect to the composer program.
            feedback.percentComplete = (goal->degrees + yaw*180.0/M_PI)/goal->degrees;
            _as.publishFeedback(feedback);
            rate.sleep();
        }
        if(success) {
            ROS_INFO_STREAM("Succeeded " << action_name);
            result.done = true;
            _as.setSucceeded(result);
        }
        //Action done - stop
        msg_cmd_vel.angular.z = 0;
        pub_cmd_vel.publish(msg_cmd_vel);
    }

    //Constructor
    //Binds executeCB to run when a SpinAction object is called.
    SpinAction(std::string name) : _as(_nh, name, boost::bind(
            &SpinAction::executeCB, this, _1), false), action_name(name), tfListener(buffer){
        _as.start();
        pub_cmd_vel = _nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
    }

    ~SpinAction(){
    }
};

int main(int argc, char **argv){
    ros::init(argc, argv, "spin_action_server");
    SpinAction spinAction("spin_action_server");
    ros::spin();
    return 0;
}

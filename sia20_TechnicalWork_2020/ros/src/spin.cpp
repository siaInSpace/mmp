/*
 *	File: spin.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/03/2020
 *
 */

#include "mmp/spin.h"
#include "mmp/action.h"
#include "mmp/json.hpp"
using json = nlohmann::json;

#include <mmp/spinAction.h>
#include <mmp/newSpinMsgAction.h>
#include "mmp/newSpinMsgAction.h"
#include <actionlib/client/simple_action_client.h>
typedef actionlib::SimpleActionClient<mmp::newSpinMsgAction> Client;


Spin::Spin(double degrees, double speed){
    this->degrees = degrees;
    this->speed = speed;
}

Spin::Spin(const Spin& other) : Spin(other.degrees, other.speed){
}

void Spin::json_load(const json &j){
    j.at("speed").get_to(this->speed);
    j.at("degrees").get_to(this->degrees);
}

std::string Spin::toString(){
    std::string s;
    s += "Spin\n";
    s += "Speed: " + std::to_string(this->speed) + "\n";
    s += "Degrees: " + std::to_string(this->degrees) + "\n";
    return s;
}

//Print result from action server
void Spin::doneCb(const actionlib::SimpleClientGoalState &state,
        const mmp::newSpinMsgResultConstPtr &result) {
    ROS_INFO("Finished in state [%s]", state.toString().c_str());
    ROS_INFO_STREAM("Error: " << result->error);
}

//Print confirmation that the action is started
void Spin::activeCb() {
    ROS_INFO_STREAM("Goal went active");
}

//Print feedback from action server
void Spin::feedbackCb(const mmp::newSpinMsgFeedbackConstPtr &feedback) {
    ROS_INFO("Spin percent complete = %03.02f", feedback->percentComplete);
}

//Get the transform for a spin
geometry_msgs::Transform Spin::get_transform() {
    geometry_msgs::Transform temp;
    tf2::Quaternion rot;
    //Rotation only in yaw.
    rot.setRPY(0, 0, -degrees*M_PI/180.0);
    rot.normalize();
    tf2::convert(rot, temp.rotation);
    return  temp;
}

//http://wiki.ros.org/actionlib_tutorials/Tutorials/Writing%20a%20Callback%20Based%20Simple%20Action%20Client
//Call the action server with goal information from this object
void Spin::act(const std::string goal_frame){
    Client ac("new_spin_action_server", true);
    ac.waitForServer();
    mmp::newSpinMsgGoal goal;
    goal.speed = speed;
    goal.goal_frame = goal_frame;

    ac.sendGoal(goal,
            boost::bind(&Spin::doneCb, this, _1, _2),
            boost::bind(&Spin::activeCb, this),
            boost::bind(&Spin::feedbackCb, this, _1));
    ac.waitForResult();
}


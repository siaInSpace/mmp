//
// Created by sia on 23/04/2020.
//

#include <mmp/newSpinAction.h>
#include <geometry_msgs/Twist.h>

//Start spin action server and link it with the execute_cb method
NewSpinAction::NewSpinAction(std::string name) : as_(nh_, name, boost::bind(
        &NewSpinAction::execute_cb, this, _1), false),
        action_name(name), tfListener(buffer), have_start_yaw(false),
        start_yaw(0), yaw(0){
    as_.start();
    pub_cmd_vel = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
}

//Gets the transform relative to the goal frame
bool NewSpinAction::update_transform(const std::string goal_frame) {
    try {
        current_transform = buffer.lookupTransform("base_link", goal_frame, ros::Time(0));
    }catch (tf2::TransformException &e){
        ROS_WARN("%s", e.what());
        ros::Duration(0.1).sleep();
        return false;
    }
    //Get the rotation to the goal pose
    tf2::Quaternion q(current_transform.transform.rotation.x,
                      current_transform.transform.rotation.y,
                      current_transform.transform.rotation.z,
                      current_transform.transform.rotation.w);
    tf2::Matrix3x3 matrix(q);
    double roll, pitch;
    matrix.getRPY(roll, pitch, yaw);
    if (!have_start_yaw){
        start_yaw = yaw;
        have_start_yaw = true;
    }
    ROS_INFO_STREAM("Angle to goal: " << yaw*180.0/M_PI);
    return true;
}

//Spin the robot
void NewSpinAction::execute_cb(const mmp::newSpinMsgGoalConstPtr &goal) {
    ros::Rate rate(100);
    ROS_INFO_STREAM("Executing new spin action to: " << goal->goal_frame
    << "with speed: " << goal->speed);
    geometry_msgs::Twist msg_cmd_vel;
    bool success = true;
    while(ros::ok()){
        //Check if action is preempted
        if(as_.isPreemptRequested()){
            ROS_INFO_STREAM("Preempted " << action_name);
            as_.setPreempted();
            success = false;
            break;
        }
        //Update transform and check if close enough to stop
        if (!update_transform(goal->goal_frame)) continue;
        if (abs(yaw) < 0.5*M_PI/180) break;

        //Make sure to move the correct way
        msg_cmd_vel.angular.z = yaw < 0 ? -goal->speed : goal->speed;
        msg_cmd_vel.angular.z = msg_cmd_vel.angular.z/100;
        ROS_INFO_STREAM("Moving at: " << msg_cmd_vel.angular.z);
        pub_cmd_vel.publish(msg_cmd_vel);

        //Calculate and publish feedback
        feedback.percentComplete = 1 - yaw/start_yaw;
        as_.publishFeedback(feedback);
        ros::spinOnce();
        rate.sleep();
    }
    //Publish result if action was successful
    if(success) {
        ROS_INFO_STREAM("Succeeded " << action_name);
        result.error = current_transform.transform;
        as_.setSucceeded(result);
    }
    //Action done - stop the robot.
    msg_cmd_vel.angular.z = 0;
    pub_cmd_vel.publish(msg_cmd_vel);
    have_start_yaw = false;
    ros::spinOnce();
}

int main(int argc, char **argv){
    ros::init(argc, argv, "new_spin_action_server");
    NewSpinAction newSpinAction("new_spin_action_server");
    ros::spin();
    return  0;
}
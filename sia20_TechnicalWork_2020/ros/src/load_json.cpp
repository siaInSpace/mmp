#include <iostream>
#include <fstream>
#include <vector>

#include <mmp/action.h>
#include <mmp/straight.h>
#include <mmp/spin.h>
#include <mmp/json.hpp>
#include <mmp/image.h>
using json = nlohmann::json;

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>

void load_actions(std::vector<Action*> &actions, std::string file_path){
    //Open file
    std::ifstream in(file_path);
    if (!in.is_open()){
        ROS_INFO_STREAM("Error loading file: " << file_path);
        exit(2);
    }
    json j;
    in >> j;
    //Load actions into vector list
    for (auto obj : j){
        std::string type = obj.at("class");
        if (type == "straight"){
            actions.push_back(new Straight(obj.get<Straight>()));
        }else if (type == "spin"){
            actions.push_back(new Spin(obj.get<Spin>()));
        }else if (type == "image"){
            actions.push_back(new Image(obj.get<Image>()));
        }
        else{
            std::cout << "Unknown type: " << type << std::endl;
        }
    }
    in.close();
}

void print_actions(std::vector<Action*> &actions){
    for (auto i = actions.begin(); i != actions.end(); i++){
        ROS_INFO_STREAM((*i)->toString());
    }
}

//Gets the transform for each actions and links them together in the transform tree
void tf_goals(std::vector<Action*> &actions){
    tf2_ros::StaticTransformBroadcaster stfBroadcaster;
    tf2_ros::TransformBroadcaster tfBroadcaster;
    tf2_ros::Buffer buffer;
    tf2_ros::TransformListener tfListener(buffer);
    ros::Duration(1).sleep(); //wait for buffer to be populated

    geometry_msgs::TransformStamped startPos;

    //Get robot position, this will be the start position for the actions
    while(true) {
        try {
            startPos = buffer.lookupTransform("odom", "base_link", ros::Time(0));
        }catch (tf2::TransformException &e){
            ROS_WARN("%s", e.what());
            ros::Duration(0.5).sleep();
            continue;
        }
        break;
    }
    startPos.child_frame_id = "goals_start_pos";
    ros::Rate rate(100);
    while (ros::ok()) {
        //ROS_INFO_STREAM("Updating goal transforms");
        startPos.header.stamp = ros::Time::now();
        tfBroadcaster.sendTransform(startPos);
        geometry_msgs::TransformStamped transform;
        //Get transforms for each action and links them together.
        for (int i = 0; i < actions.size(); i++) {
            transform.transform = actions.at(i)->get_transform();
            transform.child_frame_id = "goal_" + std::to_string(i);
            if (i == 0)
                transform.header.frame_id = "goals_start_pos";
            else
                transform.header.frame_id = "goal_" + std::to_string(i - 1);
            transform.header.stamp = ros::Time::now();
            tfBroadcaster.sendTransform(transform);
        }
        ros::spinOnce();
        //rate.sleep();
    }
}

int main(int argc, char **argv){
    ros::init(argc, argv, "json_controller");
    if (argc < 2){
        std::cout << "No file path given" << std::endl;
        exit(1);
    }
    std::vector<Action*> actions;
    load_actions(actions, argv[1]);
    print_actions(actions);

    tf_goals(actions);
    return 0
}

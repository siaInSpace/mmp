Welcome to my technical work as part of the major project 2020.

In the mission_composer directory you can find the composer part of this project.
To run this I would suggest installing qt creator on an opening this project with that. 
When in qt creater choose open project and navigate to the mission_composer.pro file, selecting this file should load the rest of the project.
If not try re running qmake from qt creator.

The ros directory contains the files used by the controller part of this project. Before running the module make sure too install all the relevant modules, I 
belive this can be done automatically with rospkg (http://wiki.ros.org/rospkg). However, I have not tested this.
A user manual detailing the commands to run is included in appendix E of the project report. However if the report is not avaliable these are the 
commands to run:
I recomend opening a new console window or tab for each point, 2.1 and 2.2 must be run in the same window. 
Also please don't forget to source the relevant devel directory.

1. roscore

2.1 export TURTLEBOT3\_MODEL=waffle 
2.2 roslaunch mmp from\_json.launch 

3. rosrun mmp load\_json <path> 

4. rosrun mmp act 

5. rosservice call /mmp\_act "json\_path: '<path>'" 

<path> should be replaced by the path to the json file created by the composer.

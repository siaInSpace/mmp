

#ifndef PAINTER_H
#define PAINTER_H

#include <QDialog>
#include <QPoint>
#include <QPointF>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QResizeEvent>
#include <QTransform>
#include <QListWidget>


class Painter: public QDialog
{
    Q_OBJECT
private:
    QListWidget *actions;
    double screen_scale;
    QPointF screen_offset;
    QTransform screen_transform;

    QPointF mouse; // current mouse location in pixel coordinates
    QPointF mapped_mouse; // current mouse location in logical coordinates
    QPointF last_mouse; // last mouse location in pixel coordinates
    QPointF mouse_diff; // last mouse motion in pixel coordinates
    QPointF mapped_mouse_diff; // last mouse motion in logical coordinates

    void updateMouse(QPointF mousePos);
    void updateScreenTransform();
    void drawGrid(QPainter *painter);

protected:
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);

public:
    Painter(QListWidget *selected_actions, QDialog* parent=0);
};

#endif

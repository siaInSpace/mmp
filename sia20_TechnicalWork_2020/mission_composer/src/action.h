/*
 *	File: action.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 23/02/2020
 *
 */

#ifndef ACTION_H
#define ACTION_H
#include <QWidget>
#include <QListWidgetItem>
#include <QTextStream>
#include <QPainter>
#include <QPoint>

#include "json.hpp"
using json = nlohmann::json;

//Abstract class for actions
class Action : public QWidget{
Q_OBJECT

private:
    virtual void set_widget_size() = 0;
protected:
    QListWidgetItem *widget_item;
    virtual void json_load(const json &j) = 0;
    virtual void json_save(json &j) const = 0;
    qreal scale_factor;
public:
    friend void to_json(json &j, const Action &obj);
    friend void from_json(const json &j, Action &obj);
    Action(QWidget *parent = 0);
    virtual ~Action();
    QListWidgetItem* get_widget_item() const;
    virtual void paint(QPainter &painter, qreal &spin, QPoint &startPos) = 0;
private slots:
    virtual void update_scale_variables();
public slots:
    virtual void update_scale_factor(qreal scale_factor);
signals:
    void scale_factor_changed();
};

//Virtual friend function from:
//https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Virtual_Friend_Function
inline void to_json(json &j, const Action &obj){
    obj.json_save(j);
}

inline void from_json(const json &j, Action &obj){
    obj.json_load(j);
}

#endif



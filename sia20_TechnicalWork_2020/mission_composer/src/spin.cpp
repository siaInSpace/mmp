/*
 *	File: spin.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 26/02/2020
 *
 */

#include <QWidget>
#include <QListWidgetItem>
#include <QDebug>
#include <QPainter>
#include <QPoint>
#include <qmath.h>

#include "action.h"
#include "spin.h"
#include "ui/ui_spin.h"


Spin::Spin(qreal radians, qreal speed, QWidget *parent) : Action(parent){
    //Setup ui
    ui = new Ui::Spin();
    ui->setupUi(this);
    set_widget_size();
    setLayout(ui->main_layout_horizontal);
    //Setup inital values
    ui->degrees_box->setValue(radians);
    ui->speed_box->setValue(speed);

}

Spin::Spin(Spin const &other) : Spin(
        other.ui->degrees_box->value(), other.ui->speed_box->value()){
}

Spin& Spin::operator=(Spin const &other){
    ui->degrees_box->setValue(other.ui->degrees_box->value());
    ui->speed_box->setValue(other.ui->speed_box->value());
    return *this;
}

Spin::~Spin(){
    delete ui;
}

void Spin::json_load(const json &j){
    qreal temp;
    j.at("degrees").get_to(temp);
    ui->degrees_box->setValue(temp);
    j.at("speed").get_to(temp);
    ui->speed_box->setValue(temp);
}

void Spin::json_save(json &j) const{
    j = json::object();
    j["class"] = "spin";
    j["degrees"] = ui->degrees_box->value();
    j["speed"] = ui->speed_box->value();
}

//Increase height of widget as to not be clipped by the list widget
void Spin::set_widget_size(){
    widget_item->setSizeHint(QSize(
            widget_item->sizeHint().width(), 60));
}

//Does not actually paint anything, but updates the spin value so that the next action
//starts in the correct direction
void Spin::paint(QPainter &painter, qreal &spin, QPoint &startPos){
    Q_UNUSED (painter)
    Q_UNUSED (startPos)
    spin = fmod(spin - ui->degrees_box->value() + 360, 360.0);
}

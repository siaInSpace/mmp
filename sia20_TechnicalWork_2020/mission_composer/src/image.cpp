#include "image.h"
#include <action.h>
#include <QDebug>
#include <ui/ui_image.h>

Image::Image(QString path, QWidget *parent) : Action(parent){
    //Setup ui
    ui = new Ui::image();
    ui->setupUi(this);
    setLayout(ui->main_layout_horizontal);
    ui->path_line_edit->setText(path);
    set_widget_size();
}

Image::Image(const Image &other) : Image(
        other.ui->path_line_edit->text()){
}

Image& Image::operator=(Image const &other){
    ui->path_line_edit->setText(other.ui->path_line_edit->text());
    return *this;
}

Image::~Image(){
    delete ui;
}

void Image::set_widget_size(){
    //Change widget height so it is not clipped by the list widget
    widget_item->setSizeHint(QSize(
        widget_item->sizeHint().width(), 60));
}


void Image::json_load(const json &j){
    std::string path;
    j.at("path").get_to(path);
    ui->path_line_edit->setText(QString::fromStdString(path));
}

void Image::json_save(json &j) const{
    j = json::object();
    j["class"] = "image";
    j["path"] = ui->path_line_edit->text().toStdString();
}

//Image has no paint action, could perhaps add a camera icon
void Image::paint(QPainter &painter, qreal &spin, QPoint &startPos){
    Q_UNUSED(painter)
    Q_UNUSED(spin)
    Q_UNUSED(startPos)
    return;
}

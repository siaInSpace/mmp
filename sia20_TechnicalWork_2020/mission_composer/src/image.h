#ifndef IMAGE_H
#define IMAGE_H

#include <action.h>
#include <ui/ui_image.h>
#include <json.hpp>
#include <QPainter>
#include <QTextStream>
#include <QString>
using json = nlohmann::json;

class Image : public Action {
private:
    Ui::image *ui;
    void set_widget_size();
protected:
    void json_load(const json &j);
    void json_save(json &j) const;
public:
    Image(QString path="image/", QWidget *parent = 0);
    ~Image();
    Image(Image const &other);
    Image& operator=(Image const &other);
    void paint(QPainter &painter, qreal &spin, QPoint &startPos);
};

#endif // IMAGE_H

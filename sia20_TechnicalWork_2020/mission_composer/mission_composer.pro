# Created using qmake (3.1), but later edited by Sindre Aalhus

QT += core gui widgets
TEMPLATE = app
TARGET = mission_composer
DESTDIR = bin/
OBJECTS_DIR = obj/
UI_DIR = src/ui/
MOC_DIR = src/moc/
DEPENDPATH += . src
INCLUDEPATH += . src

DEFINES += QT_DEPRECATED_WARNINGS

# Input
HEADERS += src/*.h
FORMS += src/ui/*.ui
SOURCES += src/*.cpp

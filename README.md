# Year3 - Major Project

The main repo for my major project during year 3 at Aberystwyth university.

For a look at the code I developed as part of this project, have a look in sia_20_TechnicalWork_2020. There you will find folders for the ros code and the gui/mission composer.

For a more genereal overview have a look at the [Project report](./project_report/sia20_ProjectReport_2020.pdf)

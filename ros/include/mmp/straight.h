/*
 *	File: straight.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/03/2020
 *
 */

#ifndef STRAIGHT_H
#define STRAIGHT_H
#include "action.h"
#include <mmp/straightAction.h>
#include <mmp/newStraightMsgAction.h>
#include <actionlib/client/simple_action_client.h>

class Straight : public Action{
    private:
        double distance;
        double speed;
        void doneCb(const actionlib::SimpleClientGoalState &state,
                const mmp::newStraightMsgResultConstPtr &result);
        void activeCb();
        void feedbackCb(const mmp::newStraightMsgFeedbackConstPtr &feedback);
        
    protected:
        void json_load(const json &j) override;
    
    public:
        std::string toString() override;
        void act(const std::string goal_frame) override;
        Straight(double distance = 0, double speed = 0);
        Straight(const Straight &other);
        geometry_msgs::Transform get_transform() override;
};

#endif



/*
 *	File: action.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/03/2020
 *
 */

#ifndef ACTION_H
#define ACTION_H
#include "json.hpp"
using json = nlohmann::json;
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

//Interface for action classes.
class Action {
    protected:
        virtual void json_load(const json &j) = 0;
        
    public:
        friend void from_json(const json &j, Action &obj);
        virtual std::string toString() = 0;
        virtual void act(const std::string goal_frame) = 0;
        virtual geometry_msgs::Transform get_transform() = 0;
};

//Virtual friend function from:
//https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Virtual_Friend_Function
inline void from_json(const json &j, Action &obj){
    obj.json_load(j);
}

#endif



//
// Created by sia on 02/04/2020.
//

#include <mmp/image.h>
#include <mmp/action.h>
#include <mmp/json.hpp>
#include <cv_bridge/cv_bridge.h>
#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>


Image::Image(std::string path) {
    this->path = path;
    done = false;
}

Image::Image(const Image &other) : Image(other.path){
}

void Image::json_load(const json &j) {
    j.at("path").get_to(path);
}

std::string Image::toString() {
    std::string s;
    s += "Image\n";
    s += "Path: " + path + "\n";
    return s;
}

//http://wiki.ros.org/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages
void Image::imageCb(const sensor_msgs::ImageConstPtr &msg){
    cv_bridge::CvImagePtr cvImagePtr;
    //Attempt to get a image, if this fails try again later
    try{
        cvImagePtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8); //OpenCV uses a weird format
    }catch (cv_bridge::Exception &e){
        ROS_ERROR("Cv_bridge exception: %s", e.what());
        done = false;
        return;
    }
    //Save image to file
    std::string image_path = "./out/";
    cv::imwrite(image_path + path + ".png", cvImagePtr->image);
    ROS_INFO_STREAM("Image written to " << image_path << path << ".png");
    done = true;
}

//Image does not move the robot
geometry_msgs::Transform Image::get_transform() {
    geometry_msgs::Transform temp;
    temp.rotation.w = 1.0;
    return temp;
}

//Subscribe to the camera feed and wait for a image to be saved.
void Image::act(const std::string goal_frame) {
    ros::NodeHandle nh;
    ros::Subscriber image_sub = nh.subscribe("/camera/rgb/image_raw", 1000,
            &Image::imageCb, this);
    while (!done){
        ros::spinOnce();
    }
}

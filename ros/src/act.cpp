//
// Created by sia on 27/04/2020.
//

#include <ros/ros.h>
#include <mmp/action.h>
#include <mmp/act.h>
#include <mmp/json.hpp>
using json = nlohmann::json;
#include <mmp/straight.h>
#include <mmp/spin.h>
#include <mmp/image.h>
#include <fstream>

//Action service, loads the actions and calls their act function.

//Load actions into a list from a file
void load_actions(std::vector<Action*> &actions, const std::string file_path){
    std::ifstream in(file_path);
    if (!in.is_open()){
        ROS_INFO_STREAM("Error loading file: " << file_path);
        exit(2);
    }
    json j;
    in >> j;
    for (auto obj : j){
        std::string type = obj.at("class");
        if (type == "straight"){
            actions.push_back(new Straight(obj.get<Straight>()));
        }else if (type == "spin"){
            actions.push_back(new Spin(obj.get<Spin>()));
        }else if (type == "image"){
            actions.push_back(new Image(obj.get<Image>()));
        }
        else{
            std::cout << "Unknown type: " << type << std::endl;
        }
    }
    in.close();
}

//The callback function loads actions and runs their act method
bool call_back(mmp::act::Request &req, mmp::act::Response &res){
    res.done = false;
    std::vector<Action*> actions;
    load_actions(actions, req.json_path);
    int goal_frame_iterator = 0;
    std::string goal_frame;
    for (auto i = actions.begin(); i != actions.end(); i++){
        goal_frame = "goal_" + std::to_string(goal_frame_iterator++);
        (*i)->act(goal_frame);
    }
    res.done = true;
    return true;
}


int main(int argc, char **argv){
    ros::init(argc, argv, "commands_act_service");
    ros::NodeHandle nh;
    ros::ServiceServer service = nh.advertiseService("mmp_act", &call_back);
    ROS_INFO_STREAM("Ready to serve!");
    ros::spin();
    return 0;
}
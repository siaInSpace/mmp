//
// Created by sia on 26/03/2020.
//
//From Robotics applications practical 2 - 2019/20

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <mmp/straightAction.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <algorithm>

//Old straight action server
class StraightAction{
private:
    geometry_msgs::TransformStamped startPos;
    geometry_msgs::TransformStamped goalPos;
    geometry_msgs::TransformStamped transform_to_goal;
    tf2_ros::TransformBroadcaster tfBroadcaster;
    tf2_ros::Buffer buffer;
    tf2_ros::TransformListener tfListener;
    tf2::Vector3 vector;
    double yaw;

    void initial_poses(double distance){
        //Wait for a start pose.
        while(true){
            try {
                startPos = buffer.lookupTransform("odom",
                                                  "base_link", ros::Time(0));
            }catch (tf2::TransformException &e){
                ROS_WARN("%s", e.what());
                //Buffer not populated, wait and try again.
                ros::Duration(0.1).sleep();
                continue;
            }
            break;
        }
        startPos.child_frame_id = "start_pos";

        //Add goal pose, distance away from start pose with no rotation.
        goalPos.transform.translation.x = distance;
        goalPos.transform.rotation.w = 1;
        goalPos.child_frame_id = "goal";
        goalPos.header.frame_id = "start_pos";
    }

    bool update_transforms(){
        //update stamp times
        startPos.header.stamp = ros::Time::now();
        tfBroadcaster.sendTransform(startPos);
        goalPos.header.stamp = ros::Time::now();
        tfBroadcaster.sendTransform(goalPos);
        ros::Duration(0.05).sleep();
        //Look for goal pose in refrence to the base link
        try{
            transform_to_goal = buffer.lookupTransform("base_link",
                                                       "goal", ros::Time(0));
        }catch (tf2::TransformException &e){
            ROS_WARN("%s", e.what());
            //Buffer not populated, wait and try again.
            ros::Duration(0.1).sleep();
            return false;
        }
        //Get a vector for the distance to goal pose
        vector = tf2::Vector3(transform_to_goal.transform.translation.x,
                              transform_to_goal.transform.translation.y,
                              transform_to_goal.transform.translation.z);

        //Get the rotation to the goal pose
        tf2::Quaternion q (transform_to_goal.transform.rotation.x,
                           transform_to_goal.transform.rotation.y,
                           transform_to_goal.transform.rotation.z,
                           transform_to_goal.transform.rotation.w);
        tf2::Matrix3x3 matrix(q);
        double roll, pitch;
        matrix.getRPY(roll, pitch, yaw);
        ROS_INFO_STREAM("Distance to goal: " << vector.length());
        ROS_INFO_STREAM("Angle to goal: " << yaw);
        return true;
    }

    //Get velocity factor to slowly ramp up speed at start
    //and ramp down speed when getting close to goal.
    double get_velocity_factor(double x, double distance){
        double velocity_factor;
        if(x >= distance-0.25){
            velocity_factor = cos(4 * M_PI * (x - distance) + M_PI) + 1;
        }else if(x < 0.25){
            velocity_factor = cos(4 * M_PI * x +M_PI) + 1;
        }else{
            velocity_factor = 2;
        }
        return velocity_factor/2;
    }

protected:
    ros::NodeHandle _nh;
    actionlib::SimpleActionServer<mmp::straightAction> _as;
    std::string action_name;
    mmp::straightFeedback feedback;
    mmp::straightResult result;
    ros::Publisher pub_cmd_vel;
public:
    void executeCB(const mmp::straightGoalConstPtr &goal){
        ros::Rate rate(100);
        ROS_INFO("Executing straight action with distance: %03.02f and speed: "
                 "%03.02f", goal->distance, goal->speed);
        geometry_msgs::Twist msg_cmd_vel;

        //Setup tf tree listener and broadcaster
        initial_poses(goal->distance);
        bool success = true;

        while (ros::ok()){
            //Check if action is preempted
            if(_as.isPreemptRequested()){
                ROS_INFO_STREAM("Preempted " << action_name);
                _as.setPreempted();
                success = false;
                break;
            }
            if (!update_transforms()) continue; //unable to update transforms re run loop.

            //Turn back towards goal
            msg_cmd_vel.angular.z = 0.0;
            if (yaw < -0.1*M_PI/180.0) msg_cmd_vel.angular.z = -0.05;
            if (yaw > 0.1*M_PI/180.0) msg_cmd_vel.angular.z = 0.05;

            //Get speed based on the velocity factor and check that the speed is not very slow or very fast.
            msg_cmd_vel.linear.x = get_velocity_factor(vector.length(), goal->distance)*goal->speed;
            if (msg_cmd_vel.linear.x < 5) msg_cmd_vel.linear.x = 1;
            if (msg_cmd_vel.linear.x > 500) msg_cmd_vel.linear.x = 500;
            msg_cmd_vel.linear.x = msg_cmd_vel.linear.x/100.0;

            ROS_INFO_STREAM("Moving at linear.x: " << msg_cmd_vel.linear.x << " and angular.z: " << msg_cmd_vel.angular.z);
            //Check if we are close enough to the goal pose
            if (vector.length() < 0.01) break;
            pub_cmd_vel.publish(msg_cmd_vel);
            //Calculate and publish feedback
            feedback.percentComplete = (goal->distance-vector.length())/goal->distance;
            _as.publishFeedback(feedback);
            rate.sleep();
        }

        if(success) {
            ROS_INFO_STREAM("Succeeded " << action_name);
            result.done = true;
            _as.setSucceeded(result);
        }
        //Action done - stop
        msg_cmd_vel.angular.z = 0.0;
        msg_cmd_vel.linear.x = 0.0;
        pub_cmd_vel.publish(msg_cmd_vel);
    }

    //Constructor
    //Binds executeCB to run when a StraightAction object is created.
    StraightAction(std::string name) : _as(_nh, name, boost::bind(
            &StraightAction::executeCB, this, _1), false), action_name(name), tfListener(buffer){
        _as.start();
        pub_cmd_vel = _nh.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
    }

    ~StraightAction(){
    }
};

int main(int argc, char **argv){
    ros::init(argc, argv, "straight_action_server");
    StraightAction straightAction("straight_action_server");
    ros::spin();
    return  0;
}


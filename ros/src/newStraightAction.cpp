//
// Created by sia on 23/04/2020.
//

#include <mmp/newStraightAction.h>
#include <geometry_msgs/Twist.h>
//Start straight action server and link it with the execute_cb method.
NewStraightAction::NewStraightAction(std::string name) : as_(nh_, name, boost::bind(
        &NewStraightAction::execute_cb, this, _1), false),
        action_name(name), tfListener(buffer), have_start_distance(false),
        start_distance(0), yaw(0){
    as_.start();
    pub_cmd_vel = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
}

//Gets the transform relative to the goal frame
bool NewStraightAction::update_transform(std::string goal_frame) {
    try {
        current_transform = buffer.lookupTransform("base_link", goal_frame, ros::Time(0));
    }catch (tf2::TransformException &e){
        ROS_WARN("%s", e.what());
        ros::Duration(0.1).sleep();
        return false;
    }
    //Get the distance
    vector_to_goal = tf2::Vector3(
            current_transform.transform.translation.x,
            current_transform.transform.translation.y,
            current_transform.transform.translation.z);
    //Get the rotation to the goal pose
    tf2::Quaternion q(current_transform.transform.rotation.x,
                      current_transform.transform.rotation.y,
                      current_transform.transform.rotation.z,
                      current_transform.transform.rotation.w);
    tf2::Matrix3x3 matrix(q);
    double roll, pitch;
    matrix.getRPY(roll, pitch, yaw);
    //Save initial distance to the goal, used to calculate feedback
    if (!have_start_distance){
        start_distance = vector_to_goal.length();
        have_start_distance = true;
        ROS_INFO_STREAM("Start distance to goal: " << start_distance);
    }
    ROS_INFO_STREAM("Distance to goal: " << vector_to_goal.length()*100 << "cm");
    return true;
}

//Get velocity factor to slowly ramp up speed at start
//and ramp down speed when getting close to goal.
double NewStraightAction::get_velocity_factor(){
    double velocity_factor;
    if(vector_to_goal.length() >= start_distance-0.25){
        velocity_factor = cos(4 * M_PI * (vector_to_goal.length() - start_distance) + M_PI) + 1;
    }else if(vector_to_goal.length() < 0.25){
        velocity_factor = cos(4 * M_PI * vector_to_goal.length() +M_PI) + 1;
    }else{
        velocity_factor = 2;
    }
    return velocity_factor/2;
}

//Move the robot
void NewStraightAction::execute_cb(const mmp::newStraightMsgGoalConstPtr &goal) {
    ros::Rate rate(100);
    ROS_INFO_STREAM("Executing straight action to: " << goal->goal_frame << " with speed: "
    << goal->speed);
    geometry_msgs::Twist msg_cmd_vel;
    bool success = true;
    while(ros::ok()){
        //Check if action is preempted
        if(as_.isPreemptRequested()){
            ROS_INFO_STREAM("Preempted " << action_name);
            as_.setPreempted();
            success = false;
            break;
        }
        //Update transform and check if clese enought to stop
        if (!update_transform(goal->goal_frame)) continue;
        if (vector_to_goal.length() < 2.0/100.0) break; //2/100 = 2cm

        //Turn back towards goal
        if (vector_to_goal.y() > 1.0/100.0) msg_cmd_vel.angular.z = 5.0/100.0;
        if (vector_to_goal.y() < -1.0/100.0) msg_cmd_vel.angular.z = -5.0/100.0;
        ROS_INFO_STREAM("Y distance: " << vector_to_goal.y());


        msg_cmd_vel.linear.x = goal->speed * get_velocity_factor();
        // limit between 5cm/s and 1 m/s
        if (msg_cmd_vel.linear.x < 5) msg_cmd_vel.linear.x = 5;
        if (msg_cmd_vel.linear.x > 100)  msg_cmd_vel.linear.x = 100;
        msg_cmd_vel.linear.x /= 100; //m/s to cm/s
        if (goal->reverse) {
            //if (vector_to_goal.x() < 0) ?
            msg_cmd_vel.linear.x *= -1;
            msg_cmd_vel.angular.z *= -1;
        }

        pub_cmd_vel.publish(msg_cmd_vel);

        //Calculate and publish feedback
        feedback.percentComplete = 1 - vector_to_goal.length()/start_distance;
        as_.publishFeedback(feedback);
        ROS_INFO_STREAM("Percent complete: " << feedback.percentComplete);
        ros::spinOnce();
        rate.sleep();
    }
    //Publish result
    if(success) {
        ROS_INFO_STREAM("Succeeded " << action_name);
        result.error = current_transform.transform;
        as_.setSucceeded(result);
    }
    //Action done - stop the robot
    msg_cmd_vel.linear.x = 0;
    msg_cmd_vel.angular.z = 0;
    pub_cmd_vel.publish(msg_cmd_vel);
    have_start_distance = false;
    ros::spinOnce();
}



int main(int argc, char **argv){
    ros::init(argc, argv, "new_straight_action_server");
    NewStraightAction newStraightAction("new_straight_action_server");
    ros::spin();
    return 0;
}

/*
 *	File: action.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 23/02/2020
 *
 */
#include <QWidget>
#include <QListWidgetItem>
#include <QDebug>

#include "action.h"

//Setup list widget and connect scale factor
Action::Action(QWidget *parent) : QWidget(parent){
    widget_item = new QListWidgetItem();
    QObject::connect(this, SIGNAL(scale_factor_changed()), this, SLOT(update_scale_variables()));
    scale_factor = 1;
}

Action::~Action(){
    delete widget_item;
}

QListWidgetItem *Action::get_widget_item() const{
   return widget_item;
}

void Action::update_scale_factor(qreal scale_factor){
    this->scale_factor = scale_factor/100.0; //Divide by 100 becuase it is a precent
    emit scale_factor_changed();
    return;
}

void Action::update_scale_variables(){
    //No values to change
    return;
}

/*
 *	File: main.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 09/02/2020
 *
 */

#include <QApplication>
#include <QMainWindow>
#include "ui/ui_main_window.h"

#include "mainWindow.h"


int main(int argc, char **argv){
    QApplication app(argc, argv);
    MainWindow *mw = new MainWindow();
    mw->show();
    return app.exec();
}

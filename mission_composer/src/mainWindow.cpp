/*
 *	File: mainWindow.cpp
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 12/02/2020
 *
 */

#include <QMainWindow>
#include <QVector>
#include <QListWidget>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QCloseEvent>
#include <QFileDialog>
#include <QDir>

#include <typeinfo>

#include "mainWindow.h"
#include "action.h"
#include "straight.h"
#include "spin.h"
#include "json.hpp"
#include <image.h>
using json = nlohmann::json;
#include <fstream>
#include <iomanip>
#include <painter.h>

MainWindow::MainWindow() : QMainWindow(){
    ui.setupUi(this); //setup ui
    //Setup actions for availiable actoins
    Action *straight = new Straight();
    Action *spin = new Spin();
    Action *image = new Image();
    //Add to list widgets and add the widget
    ui.available_actions->addItem(straight->get_widget_item());
    ui.available_actions->setItemWidget(straight->get_widget_item(), straight);
    ui.available_actions->addItem(spin->get_widget_item());
    ui.available_actions->setItemWidget(spin->get_widget_item(), spin);
    ui.available_actions->addItem(image->get_widget_item());
    ui.available_actions->setItemWidget(image->get_widget_item(), image);
    //Add drag and drop behaviour to selected actions
    ui.selected_actions->setDragDropMode(QAbstractItemView::InternalMove);


    //Connect buttons to actions
    QObject::connect(ui.add_to_seleced, SIGNAL(clicked(bool)), this,
            SLOT(add_selected()));
    QObject::connect(ui.remove_from_selected, SIGNAL(clicked(bool)), this,
            SLOT(remove_selected()));
    QObject::connect(ui.available_actions, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
            this, SLOT(add_selected()));
    QObject::connect(ui.actionOpen, SIGNAL(triggered()), this, SLOT(load()));
    QObject::connect(ui.actionSave, SIGNAL(triggered()), this, SLOT(save()));
    QObject::connect(ui.actionShow, SIGNAL(triggered()), this, SLOT(show_path()));
}

//Slots

//Add selected action in available actions to
//selected actions
void MainWindow::add_selected(){
    Action *temp = (Action*)ui.available_actions->itemWidget(
            ui.available_actions->currentItem());
    std::type_info const &type = typeid(*temp);
    if(type == typeid(Straight)){
        temp = new Straight(*(Straight*)temp);
        //qDebug() << "Is straight";
    }else if (type == typeid(Spin)){
        temp = new Spin(*(Spin*)temp);
        //qDebug() << "Is Spin";
    }else if (type == typeid(Image)){
        temp = new Image(*(Image*)temp);
        //qDebug() << "Is Image";
    }else{
        qDebug() << "Unknown type";
        delete temp;
        return;
    }
    ui.selected_actions->addItem(temp->get_widget_item());
    ui.selected_actions->setItemWidget(temp->get_widget_item(), temp);
    QObject::connect(ui.scale_factor_spin_box, SIGNAL(valueChanged(double)), temp, SLOT(update_scale_factor(qreal)));
}

void MainWindow::remove_selected(){
    delete ui.selected_actions->itemWidget(ui.selected_actions->takeItem(
                ui.selected_actions->currentRow()));
}


//Opens a window to select file to load then 
//loads the data into the program
void MainWindow::load(){
    QString file_path = QFileDialog::getOpenFileName(this, "Load file", "", 
            "Json(*.json)");
    std::ifstream i(file_path.toStdString());
    if (!i.is_open()) return;
    //clear selected actions
    while(ui.selected_actions->count())
        delete ui.selected_actions->itemWidget(
                ui.selected_actions->takeItem(0));

    json j;
    i >> j;
    Action *temp;
    for (auto obj : j){
        QString cl = QString::fromStdString(obj.at("class"));
        if (cl == "straight"){
            temp = new Straight(obj.get<Straight>());
        }else if (cl == "spin"){
            temp = new Spin(obj.get<Spin>());
        }else if(cl == "image"){
            temp = new Image(obj.get<Image>());
        }else{
            temp = NULL;
            qDebug() << "Unkown type: " << cl;
            continue;
        }
        ui.selected_actions->addItem(temp->get_widget_item());
        ui.selected_actions->setItemWidget(temp->get_widget_item(), temp);
        QObject::connect(ui.scale_factor_spin_box, SIGNAL(valueChanged(double)), temp, SLOT(update_scale_factor(qreal)));
    }
}

//Save selected actions to a file
void MainWindow::save(){
    QString file_path = QFileDialog::getSaveFileName(this, "Save file", "", 
            "Json(*.json)");
    Action *temp;
    json j = json::array();
    for (qint32 i = 0; i < ui.selected_actions->count(); i++){
        temp = (Action*)ui.selected_actions->itemWidget(
                ui.selected_actions->item(i));
        j.push_back(*temp);
    }
    std::ofstream o(file_path.toStdString());
    if(!o.is_open()) return;
    o << std::setw(2) << j << std::endl;
    o.flush();
    o.close();
}

void MainWindow::show_path(){
    Painter p(ui.selected_actions);
    p.exec();
}


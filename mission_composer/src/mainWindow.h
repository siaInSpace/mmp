/*
 *	File: mainWindow.h
 *	
 *	Author: “SINDREAALHUS” 
 *	Date: 12/02/2020
 *
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QCloseEvent>

#include "straight.h"
#include "ui/ui_main_window.h"

class MainWindow : public QMainWindow{
Q_OBJECT


private:
    Ui::main_window ui;

public:
    MainWindow();

private slots:
    void add_selected();
    void remove_selected();
    void load();
    void save();
    void show_path();
};

#endif



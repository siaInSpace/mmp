//From https://www.qtcentre.org/threads/50021-Adding-Scale-in-Qt-Painting
#include "painter.h"
#include <action.h>
#include <QPen>
#include <QDebug>

Painter::Painter(QListWidget *selected_actions, QDialog *parent) : QDialog(parent){
    actions = selected_actions;
    screen_scale = 1;
    screen_offset = QPointF(0,0);
}

//Translate with mouse
//Change mouse icon if the right mouse button is pressed
//to indicate translation mode
void Painter::mousePressEvent(QMouseEvent *event){
    if(event->buttons() & Qt::RightButton){
        updateMouse(event->pos());
        setCursor(Qt::ClosedHandCursor);
    }
}

//Get mousemovement and translate the window
void Painter::mouseMoveEvent(QMouseEvent *event){
    updateMouse(event->pos());
    if (event->buttons() & Qt::RightButton){
        screen_offset -= mapped_mouse_diff;
        updateScreenTransform();
    }
    update();
}

//Update mouse icon when button is released
void Painter::mouseReleaseEvent(QMouseEvent *event){
    updateMouse(event->pos());
    unsetCursor();
    update();
}

//Update the screen transform every time a rezise event happens
void Painter::resizeEvent(QResizeEvent* event){
    Q_UNUSED(event);
    updateScreenTransform();
}

//Update mouse posionton
void Painter::updateMouse(QPointF mouse){
    mapped_mouse = screen_transform.inverted().map(mouse);
    mouse_diff = (mouse - last_mouse);
    mapped_mouse_diff = mouse_diff*screen_scale;
    mapped_mouse_diff.ry() = -mapped_mouse_diff.y();
    last_mouse = mouse;
}

//Update the transform
void Painter::updateScreenTransform(){
    screen_transform = QTransform();
    screen_transform.translate(width()/2, height()/2); // Place the origin in the center of the screen.
    screen_transform.scale(1.0/screen_scale, -1.0/screen_scale); // Flip it upside down and scale (zoom) it.
    screen_transform.translate(-screen_offset.x(), -screen_offset.y()); // Apply the offset.
    mapped_mouse = screen_transform.inverted().map(mouse); //Calculate the logical mouse coordinates.
    update();
}
//End translation

//Scale window
void Painter::wheelEvent(QWheelEvent *event){
    if (event->delta() > 0){
        screen_scale /= 1.2;
    }else{
        screen_scale *= 1.2;
    }
    updateScreenTransform();
    updateMouse(QPointF(event->pos()));
    update();
}

void Painter::drawGrid(QPainter *painter){
    static qint16 meter_scale = 1;
    qreal meter = meter_scale * 100 / screen_scale;
    //Check how many pixels a meter is
    if (meter < 40) meter_scale++; //Too short -> increase meter scale
    if (meter > 100) meter_scale--;//Too long -> decrease meter scale
    if (meter_scale < 1) meter_scale = 1; //Limit meter scale to >= 1
    qint32 rows = width() / meter;
    qint32 collums = height() / meter;

    //Draw the grid
    QPen pen(Qt::gray, 1);
    painter->setPen(pen);
    for (qint16 i = 0; i < collums+1; i++){
        painter->drawLine(QPointF(0, i*meter), QPointF(width(), i*meter));
    }
    for (qint16 i = 0; i < rows+1; i++){
        painter->drawLine(QPointF(i*meter, 0), QPointF(i*meter, height()));
    }
    //Print a text showing the meter scale
    painter->drawText(width() - 80, height() - 10, QString::number(meter_scale) + "m x " + QString::number(meter_scale) + "m");
    update();
}

//Main paint method
void Painter::paintEvent(QPaintEvent*){
    QPainter painter(this);
    //Draw grid and transform screen based on translation with mouse
    drawGrid(&painter);
    painter.setTransform(screen_transform);
    QPen pen(Qt::black, 3);
    painter.setPen(pen);
    //Start with offset to be in same direction as controller
    qreal spin = 90;
    //Draw the actions
    QPoint currentPoint;
    Action *temp;
    qint32 count = actions->count();
    for (qint32 i = 0; i < count; i++){
       temp = (Action*)actions->itemWidget(actions->item(i));
       temp->paint(painter, spin, currentPoint);
    }
    //Paint end point
    pen.setColor(Qt::blue);
    pen.setWidth(5);
    painter.setPen(pen);
    painter.drawPoint(currentPoint);
    //Paint startpoint
    pen.setColor(Qt::green);
    painter.setPen(pen);
    painter.drawPoint(0, 0);
}

/*
 *	File: straight.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 10/02/2020
 *
 */

#ifndef STRAIGHT_H
#define STRAIGHT_H
#include <QWidget>
#include <QListWidgetItem>
#include <QTextStream>
#include <QPainter>
#include <QPoint>

#include "action.h"
#include "ui/ui_straight.h"
#include "json.hpp"
using json = nlohmann::json;

class Straight : public Action{
Q_OBJECT
private:
    Ui::Straight *ui;
    void set_widget_size();
    qreal distance;
protected:
    void json_load(const json &j);
    void json_save(json &j) const;
public:
    Straight(qreal distance = 0.0, qreal speed = 0.0, QWidget *parent = 0);
    ~Straight();
    Straight(Straight const &other);
    Straight& operator=(Straight const &other);
    void paint(QPainter &painter, qreal &spin, QPoint &startPos);
public slots:
    void on_distance_box_valueChanged(double value);
    void update_scale_variables() override;
};


#endif



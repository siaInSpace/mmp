/*
 *	File: spin.h
 *	
 *	Author: “SINDRE AALHUS” 
 *	Date: 26/02/2020
 *
 */

#ifndef SPIN_H
#define SPIN_H
#include <QWidget>
#include <QListWidgetItem>
#include <QTextStream>
#include <QPainter>
#include <QPoint>
#include "action.h"
#include "ui/ui_spin.h"
#include "json.hpp"
using json = nlohmann::json;

class Spin : public Action{
private:
    Ui::Spin *ui;
    void set_widget_size();

protected:
    void json_load(const json &j);
    void json_save(json &j) const;

public: 
    Spin(qreal radians = 0.0, qreal speed = 0, QWidget *parent = 0);
    ~Spin();
    Spin(Spin const &other);
    Spin& operator=(Spin const &other);
    void paint(QPainter &painter, qreal &spin, QPoint &startPos);
};

#endif



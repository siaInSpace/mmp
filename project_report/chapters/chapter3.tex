\chapter{Implementation}
\section{Composer}
\subsection{GUI \& Lists}
As described in \autoref{sec:analysis} the GUI uses to Qt framework
to display a window on the screen.
Through the use of the Qt framework it should be easy to design the GUI 
with lists of actions due to the framework already having developed a 
program to design GUIs and develop their behaviour\cite{qt_creator}.
The program creates a XML 
document of your window and has a parser to create a class usable in 
your program. Initially I wanted to have more control over the 
sets of actions, as to better maintain an understanding of how everything
fit together. By following this I implemented the lists myself by using
signals sent by Qt to populate a C++ vector, this vector was then
used to fill the list widget to display on the GUI. This ultimately 
resulted in the same object being handled directly by my program and 
by the Qt framework. This implementation resulted in many bugs where 
actions would not display properly and some much dreaded segmentation faults. In the end it was decided
to give some control away and let the framework handle the GUI as much
as possible. Initially
it was difficult to understand how Qt wanted to do things, but after
reading some documentation on the Qt wiki\cite{qt_docs} explaining 
how the class should be used it all worked better.

\subsection{Actions}
Each action has two major components. The first component contains data relevant to the 
action type and, the second is a user interface. The user interface needs
to allow the user too interact with the relevant data to each action while also
interfacing with the list widget. This is done to bridge the gap
between a action and the list widget.
Each sub-class defines its user interface and then
using the super-class to connect this user interface to the list widget.
This is achieved by using  a QListWidgetItem as part of the action class.
The QListWidgetItem is a unique identifier for an item in a list widget.

\subsubsection{Scaling values}
Some objects can scale their values. For now the distance of a 
straight is the only one that is scalable. For a variable to be scalable the object requires
an additional variable to keep track of the unscaled value. This is 
used in conjunction with the scale value to give the correct value on 
screen and is updated as the scale value is updated. This is done
using the Signal-Slot system implemented by Qt. The Signal-Slot system
as part of Qt is used to linked methods together. When a signal is 
emitted for one object, for example when the user changes the scale
factor another method is called with the information from the signal.
Using this Signal-Slot system every scale value can be updated by
linking it to the signal from the scale factor. 

\subsection{Path Painter}
The painter class handles displaying the path a robot should take 
following the current list of actions in the selected actions list
widget.
This is done by using the QPainter class from Qt and iterating over the
set of actions having each of the actions paint the path they should take. To chain
these together two variables are used, a point that keeps track of
where the next action should start painting from, and a spin value
that keeps track of the direction the robot is facing. An example
path can be seen in \autoref{img:gui_path}. Through this feature
the user can easily see the path a robot should take following the
actions specified, this helps spot any error and makes it possible to 
make changes before the actions is acted out by the robot. This helps
reduce the risk of a creating a plan that can damage the robot.

\subsubsection{Spin}
The paint method in the spin action does not actually paint anything
as the robot is not moving in any direction, rather it is rotating there
is nothing to paint.
However, it does update the spin value used by the other actions. This
makes sure the robot faces the correct direction when drawing the 
path of the other actions.

\subsubsection{Straight}
\autoref{app:straight_paint} shows how the paint method was implemented
in the straight class.
Firstly the spin variable is used to calculate the x and y components,
this is then used to define the endpoint as a distance from
the 'startPos' variable. Then the line is draw on screen and the
startPos variable is updated to make sure the next action will start
from the correct position.

\section{Interface}
As discussed in \autoref{sec:interface} the interface uses a json format
to store the data. The conversion from the composer to the controller
is done similarly in both parts of the project. 
As show in \autoref{fig:comp_uml} each action in the composer has a 
json\_load() and a json\_save() method. Similarly, in \autoref{fig:cont_imp}, 
each action in the controller has a json\_load() method.
However, a json\_save() method is not needed for the controller as it does not 
change the mission plan. Each action will create a new object that will 
be added to the list of objects. Henceforth the structure is kept simple; 
each object has an object\_name variable that details the specific
action type, 'straight', 'spin' or 'image. This is followed by any variables associated with the action such as distance and speed for 
a straight object.
A section of the resulting json file derived from the actions selected in  
\autoref{img:main_window_actions} can be seen in \autoref{app:json}.

For each action the json\_load() is implemented similarly, or in the
same way.
This is done by first reading
the json file into a json object using the JSON for modern C++ library 
\cite{json_lib}. Each object then loads the data associated with its 
class.
Subsequently, json\_save() works very much in the same way, but in reverse.
A json object is created and each action in the selected set feeds
this object with their data. Then the object parses the data to a text
file before saving it using the Qt framework. This file is then passed to 
the controller to act out the list actions, or it is loaded back into 
the composer for edits.


\section{Controller}
\input{tikz_diagrams/controller_implementation_diagram}
\subsection{Transform tree}
The transform tree is a module in ROS, called tf2\cite{tf2}. The module handles 
coordinate frames in relation to each other. A transform tree
is usually used to define links and parts of a robot, but can also
be used for more complex work. In this project tf2 has been used
to define the goal position of each action, this is done by
chaining transforms together. Because of this, a map of the actions
can be created (see \autoref{img:gui_path}). As shown in \autoref{fig:cont_imp}, a list of each action
is created. Following this get\_transform() method is called. get\_transform() returns
the transform to the goal for that actions. This is done iteratively 
through the list which is what creates the map so that the progress made
by the robot could be evaluated in a visual manner.

When an action needs to know the position in relation to a goal, the 
transform tree is used, shown in \autoref{fig:st_sp_imp}. This removes
the need to manually keep track of the odometry data from the robot as
the tf2 module handles this on its own.

\subsection{Action servers}
The act method for the spin and straight actions are both implemented 
with an action server from the actionlib module of ROS\cite{actionlib}.
An action server is given a goal when executed the server publishes feedback as it runs.
Then when the action server completes the action a result is published.
This is ideal to use for something that might take some time and you want
an overview of what is happening along the way.

The spin and straight action servers are implemented in very much the 
same way. 
When a spin or straight object calls its act() method it sends
a goal to the appropriate action server with information on the specific
object. 
When the information has been received the action server takes 
over. 
The spin and straight
action servers are outlined in \autoref{fig:st_sp_imp}, but here is a 
more detailed description:

First an initial
distance to the goal is retrieved from the transform tree. This is later
used to calculate the feedback value. 
Following this, a loop is started. The loop
first checks if the robot is at or close to the end goal, if not the action
server moves the robot towards the goal using the information sent
to it from the action object. 
The feedback value for both the straight and spin
is a percentage of how far the robot has moved toward the goal. 
The goal is used to keep track of where to robot should move towards.
Then the position is updated using the transform tree and the loop runs
again. If the robot is deemed close enough to the goal, the result is 
published in the form of the distance the robot had to the goal when it 
stopped. After which the action server is completed and it exits.
\input{tikz_diagrams/straight_spin_implementation_diagram}

\subsubsection{Action messages}
\input{code_files/messages}
Action messages are what defines which types of information an action 
server needs. \autoref{code:straightMsg} shows how the straight action
is defined in this project. The goal\_frame specifies which goal to move
towards, while the speed specifies how fast the robot should move. 
The one difference between the spin and straight action message is
the reverse flag. This details if the of a straight action goal is behind the robot and
therefore letting it know if it should move in reverse. The result of 
this message which is published when the action
has completed, is a Transform error. This message details how far away 
the robot is 
from the goal when it decided it was close enough to stop. This is used
to give a clear indication on how well the action preformed.
Finally
there is the feedback. The feedback is a percentage calculated by
how far the robot has moved towards the goal. The feedback is constantly
published to keep track of the progress made by the action server.


\subsection{Image}
The image action receives the camera feed from the robot and is not implemented with an action server. This is
because it is not a lengthy action and should take little to no time. Instead the image just loops while it waits for a callback from the 
camera publisher. When an image from the camera feed is received, the 
image object saves the current image to a file with its specified path 
and then exits. This is useful as it makes it possible to save images
from different locations that can then be studied at a later time.



/*
 *	File: straight.cpp
 *
 *	Author: “SINDRE AALHUS”
 *	Date: 10/02/2020
 *
 */

#include <QWidget>
#include <QListWidgetItem>
#include <QDebug>
#include <QTextStream>
#include <QPainter>
#include <QPoint>
#include <QtMath>

#include "action.h"
#include "straight.h"
#include "ui/ui_straight.h"
#include "json.hpp"
using json = nlohmann::json;
#include <iostream>

void Straight::json_load(const json &j){
    qreal temp;
    j.at("distance").get_to(temp);
    ui->distance_box->setValue(temp);
    j.at("speed").get_to(temp);
    ui->speed_box->setValue(temp);
}

void Straight::json_save(json &j) const{
    j = json::object();
    j["class"] = "straight";
    j["distance"] = ui->distance_box->value();
    j["speed"] = ui->speed_box->value();
}

Straight::Straight(qreal distance, qreal speed, QWidget *parent) : Action(parent){
    //Setup ui
    ui = new Ui::Straight();
    ui->setupUi(this);
    setLayout(ui->main_layout_horizontal);
    set_widget_size();
    //Setup inital values
    this->distance = distance;
    ui->distance_box->setValue(distance);
    ui->speed_box->setValue(speed);
    //Connect for scaling
    QObject::connect(ui->distance_box, SIGNAL(valueChanged(double)), this, SLOT(on_distance_box_valueChanged(double)));
}

void Straight::set_widget_size(){
    widget_item->setSizeHint(QSize(
            widget_item->sizeHint().width(), 60));
}

Straight::Straight(Straight const &other) : Straight(
        other.ui->distance_box->value(), other.ui->speed_box->value()){
}

Straight::~Straight(){
    delete ui;
}

Straight& Straight::operator=(Straight const &other){
    ui->speed_box->setValue(other.ui->speed_box->value());
    ui->distance_box->setValue(other.ui->distance_box->value());
    return *this;
}

//Paint a line
void Straight::paint(QPainter &painter, qreal &spin, QPoint &startPos){
    //Calculate x and y distance
    qreal xDiff = ui->distance_box->value()*100 * qCos(qDegreesToRadians(spin));
    qreal yDiff = ui->distance_box->value()*100 * qSin(qDegreesToRadians(spin));
    QPoint endPoint;
    endPoint.setX(startPos.x() + xDiff);
    endPoint.setY(startPos.y() + yDiff);
    painter.drawLine(startPos, endPoint);
    //Update startpoint such that the next action will start from the correct position.
    startPos.setX(endPoint.x());
    startPos.setY(endPoint.y());
}

//Scaling distance value
void Straight::on_distance_box_valueChanged(double value){
    distance = value/scale_factor;
    //qDebug() << "Updating distance value: " << value << scale_factor << distance;
    emit scale_factor_changed();
}

void Straight::update_scale_variables(){
    ui->distance_box->setValue(distance * scale_factor);
    //qDebug() << "Updating scale variable: " << distance << scale_factor << distance*scale_factor;
}
